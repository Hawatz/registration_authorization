<?php
require_once('xmldb.php');
function json_response($success){
    echo json_encode(array('success' => $success));
    die;
}
if(isset($_POST['login'])){
    $login = preg_replace('/[^A-Za-z]/', '', $_POST['login']);
    if($login == '' || $_POST['password'] == ''){
        json_response(0);
    }
    $db = xmlDb::connect('users_db');
    $db->from('user_data')
        ->select('sault')
        ->where('login', $login);
    $sault = $db->getRow()->sault;
    $password = md5($_POST['password'] . $sault);
    if($sault){
        $db = xmlDb::connect('users_db');
        $db->from('user_data')
            ->select('password')
            ->where('login', $login);
        $db_password = $db->getRow()->password;
        if($password == $db_password){
            session_start();
            $db = xmlDb::connect('users_db');
            $db->from('user_data')
                ->select('name')
                ->where('login', $login);
            $name = $db->getRow()->name;
            $db = xmlDb::connect('users_db');
            $db->in('user_data')
                ->where('login', $login)
                ->bind('session_id', session_id())
                ->update();
            $_SESSION['name'] = $name;
            json_response(1);
        }
    }
}
