<?php
require_once('xmldb.php');
session_start();
$db = xmlDb::connect('users_db');
$db->from('user_data')
    ->select('session_id')
    ->where('name', $_SESSION['name']);
    $session_id = $db->getRow()->session_id;
    if($session_id != session_id()){
        header('Location: login.html');
        die;
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>User Page</title>
</head>
<body>
    <h1>User Page</h1>
    <h2>Hello, <?php echo $_SESSION['name']; ?></h2>
    <hr />
    <a href="logout.php">Logout</a>
</body>
</html>
