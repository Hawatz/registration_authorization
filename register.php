<?php
require_once('xmldb.php');
$db = xmlDb::connect('users_db');
$db->addTable('user_data');
$errors = array();
function json_response($success){
    echo json_encode(array('success' => $success));
    die;
}
if(isset($_POST['login'])){
    $db = xmlDb::connect('users_db');
    $login = preg_replace('/[^A-Za-z]/', '', $_POST['login']);
    $email = $_POST['email'];
    $password = $_POST['password'];
    $c_password = $_POST['c_password'];
    $name = $_POST['name'];
    $db->from('user_data')
        ->select('login, email')
        ->where('login', $login)
        ->orWhere('email', $email);
    $data = $db->getAll();
    if($data){
        $errors[] = 'Login and/or email already taken';
    }
    if($login == ''){
        $errors[] = 'Login is blank';
    }
    if($email == ''){
        $errors[] = 'Email is blank';
    }
    if($password == '' || $c_password == ''){
        $errors[] = 'Passwords are blank';
    }
    if($password != $c_password){
        $errors[] = 'Passwords do not match';
    }
    if($name == ''){
        $errors[] = 'Name is blank';
    }
    if(count($errors) == 0){
        $db = xmlDb::connect('users_db');
        $sault = uniqid();
        $password = md5($password . $sault);
        $info = [
                'login' => $login,
                'email' => $email,
                'password' => $password,
                'sault' => $sault,
                'name' => $name,
                'session_id' => '',
            ];
        $db->in('user_data')->insert($info);
        json_response(1);
    } else {
        json_response($errors);
    }
}
