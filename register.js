$(document).ready(function() {
    $('#registerform').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'register.php',
            data: $(this).serialize(),
            success: function(response)
            {
                let jsonData = JSON.parse(response);
                if (jsonData.success == "1")
                {
                    location.href = 'login.html';
                }
                else
                {
                    alert(jsonData.success);
                }
            }
        });
    });
});
